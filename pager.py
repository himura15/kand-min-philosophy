# encoding: utf-8

file_name = "Философия техники.txt"

src = open(file_name, encoding='utf-8').read()
page_length = 20000  # 10 kB
pages = src.split("\n\n---\n\n")


def fill(text, length, symbol):
    if text[-1] == "=":
        length //= 2

    text_size = len(text)
    fill_size = length - text_size

    if fill_size < 0:
        print("Page overflow on the following page:")
        print(text.strip().split('\n')[0])
        print(text.strip().split('\n')[-1])
        return text[:length]
    return "%s\n%s" % (text, symbol * fill_size)


ret = "\n".join([fill(page, page_length, ' ') for page in pages])

dest_file = (lambda a: "%s_paged.%s" % tuple(a))(file_name.rsplit('.', 1))

with open(dest_file, 'w', encoding='utf-8') as f:
    f.write(ret.strip())
